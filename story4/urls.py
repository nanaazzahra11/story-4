from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.Page1, name='Page1'),
    path('Page2/', views.Page2, name='Page2'),
    path('Page3/', views.Page3, name='Page3'),
    path('Page4/', views.Page4, name='Page4'),
    path('Page5/', views.Page5, name='Page5'),
    path('Page6/', views.Page6, name='Page6'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete'),

]
