from django.shortcuts import render, redirect
from .models import Schedule
from .import forms

# Create your views here.
def Page1(request):
    return render(request, 'Page1.html')

def Page2(request):
    return render(request, 'Page2.html')

def Page3(request):
    return render(request, 'Page3.html')

def Page4(request):
    return render(request, 'Page4.html')

def Page5(request):
    return render(request, 'Page5.html')

def Page6(request):
    return render(request, 'Page6.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story4:schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request):
	Schedule.objects.all().delete()
	return render(request, "schedule.html")

